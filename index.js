var express = require("express");
var session = require('express-session');
var passport = require("passport");
var LocalStrategy = require('passport-local').Strategy;
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require("./config");
var admin = require("./routes/admin");
var db = require('mysql');
var path = require("path");
var connection = db.createConnection({
  host     : config.db_host,
  user     : config.db_user,
  password : config.db_pswd,
  database : config.database
});

var app = express();
app.listen(config.port);

connection.connect(function(err){
    if(err) {
        console.log("Error connecting " + config.database);    
    }
});
app.use(express.static("public"));
app.use(function (req, res, next) {
    console.log(req.method, req.url);
    next();
});

app.use(cookieParser());
app.use(bodyParser());

app.use(session({ secret: config.session_secret }));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    connection.query('SELECT * from user where id = ?', [id], function(err, rows, fields) {
        return done(err, rows[0]);
    });
});

passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true,
    },
    function(req, email, password, done) {
        connection.query('SELECT * from user where email = ?', [email], function(err, rows, fields) {
            if (err) {
                console.log(err);
                return done(null, false);
            
            }
            if (rows.length === 0 || rows[0].password !== password) {
                return done(null, false);
            }
            
            return done(null, rows[0]);
        });
    }
));

app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname + "/templates/index.html"));
});

app.get("/login", function (req, res) {
    res.sendFile(path.join(__dirname+'/login.html'));
});

app.get("/houses", function (req, res) {
    connection.query('SELECT * from flat', function(err, rows, fields) {
        if (err) {
            res.json({ err: err });
        } else {
            res.json(rows);
        }
    });
});

app.post('/login', passport.authenticate('local-login', {
     successRedirect : '/profile',
     failureRedirect : '/login',
}));

app.use("/", admin);