var express = require("express");
var router = express.Router();

var isLoggedIn = function (req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

router.get("/profile", isLoggedIn, function (req, res) {
    res.send("profile");
});

module.exports = router